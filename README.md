# gmail-string-query

> Simulate GMail search queries using Lucene Filters

Fork of [finwo/lucene-filter](https://gitlab.com/finwo/lucene-filter).

Changes:

- implicit operator is **AND** not **OR**
- support for `label:name` queries
- depends on `lucene-query-parse` only

## Installation

```bash
npm i -s gmail-string-query
yarn add gmail-string-query
```

## Example

```javascript
import * as query from "gmail-string-query";

const data = [
  {
    id: 1,
    to: "someone1@gmail.com",
    from: "sender1@gmail.com",
    // labels have to be a string separated by commas under the `label` key
    label: "foo,bar,!s-baz"
  },
  {
    id: 2,
    to: "someone1@gmail.com",
    from: "sender1@gmail.com",
    label: "foo"
  },
  {
    id: 3,
    to: "someone2@gmail.com",
    from: "sender1@gmail.com",
    label: "!s-baz"
  }
];

const result = data.filter(
  query("to:someone2@gmail.com OR (label:!s-baz label:foo)")
);
console.log(result.map(r => r.id)); // -> [1, 3]
```

## Why?

Part of GMail API mock used in [taskbot](https://github.com/TaskSync/taskBot.app)
