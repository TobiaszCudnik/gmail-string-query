const parser = require("lucene-query-parser");

const lucene = (module.exports = function compiler(query) {
  // Parse string query
  if (!query) return () => 0;
  if ("string" === typeof query) {
    try {
      query = parser.parse(luceneToGmailQuery(query));
    } catch (e) {
      return () => 0;
    }
  }

  // Compile combined
  if (query.operator) {
    return lucene.operators[query.operator](
      lucene(query.left),
      lucene(query.right)
    );
  }

  // Wrapped
  if (query.left) {
    return lucene(query.left);
  }

  // Ensure default boost
  query.boost = query.boost || 1;

  // Return the first detected filter
  for (const filter of lucene.filters) {
    if (filter.detect(query)) {
      return filter.compile(query);
    }
  }

  // Return no match
  return () => 0;
});

function luceneToGmailQuery(query) {
  // change label queries into regexps
  return query.replace(/label:([\w-!]+)/g, "label:/(,|^)$1(,|$)/");
}

// Return the data when matching
// TODO: lucene() = { f() passthrough: f() }
lucene.passthrough = function(query) {
  let match = lucene(query);
  return function(data) {
    if (match(data)) return data;
    return undefined;
  };
};

// Add filters & operators
lucene.filters = require("./filters");
lucene.operators = require("./operators");

// Browser exports
if ("function" === typeof define && define.amd) {
  define(() => lucene);
} else if ("object" === typeof window) {
  window.lucene = lucene;
}
