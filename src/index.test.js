import expect from "expect";
const query = require("./index");

const data = [
  {
    to: "someone1@gmail.com",
    from: "sender1@gmail.com",
    label: "foo,bar,!s-baz"
  },
  { to: "someone1@gmail.com", from: "sender1@gmail.com", label: "foo" },
  { to: "someone2@gmail.com", from: "sender1@gmail.com", label: "!s-baz" }
];

test("queries by a single label", async () => {
  const result = data.filter(query("label:foo"));
  expect(result).toHaveLength(2);
});

test("queries by multiple labels", async () => {
  const result = data.filter(query("label:foo OR label:bar"));
  expect(result).toHaveLength(2);
});

test("queries by special character labels", async () => {
  const result = data.filter(query("label:foo AND label:!s-baz"));
  expect(result).toHaveLength(1);
});

test("queries by a sender", async () => {
  const result = data.filter(query("from:sender1@gmail.com"));
  expect(result).toHaveLength(3);
});

test("queries by a recipient", async () => {
  const result = data.filter(query("to:someone1@gmail.com"));
  expect(result).toHaveLength(2);
});

test("queries by a sender and a recipient", async () => {
  const result = data.filter(
    query("from:sender1@gmail.com AND to:someone1@gmail.com")
  );
  expect(result).toHaveLength(2);
});

test("queries by a nested implicit operator", async () => {
  const result = data.filter(
    query("to:someone2@gmail.com OR (label:!s-baz label:foo)")
  );
  expect(result).toHaveLength(2);
});
